//
//  ViewController.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController<MainView, MainViewModel> {

	// MARK: - LifeCycle
	override func viewDidLoad() {
		super.viewDidLoad()
		viewModel.get()
	}

	// MARK: - BindData
	override func bindData() {
		super.bindData()

		subview.tableView.rx.itemSelected
			.subscribe(onNext: { [weak self] indexPath in

				guard let self = self, indexPath.section == 1, let cell = self.subview.tableView.cellForRow(at: indexPath) as? CountryCell else { return }
				let viewController = CountryViewController()
				viewController.countryName = cell.name.text ?? ""
				self.present(viewController, animated: true, completion: nil)
				self.subview.tableView.deselectRow(at: indexPath, animated: true)
			})
			.disposed(by: bag)
	}

	override func setupViews() {
		super.setupViews()
		subview.refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
	}

	@objc
	func refreshData() {
		viewModel.get()
		subview.refresh.endRefreshing()
	}
}
