//
//  MainView.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import UIKit

class MainView: BaseTableView<MainViewModel> {

	public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

		let headerView: UIView = {
			let view = UIView()
			view.backgroundColor = UIColor(named: "csWhite")
			return view
		}()

		let headerLabel: UILabel = {
			let label = UILabel()
			label.text = dataSource.sectionModels[section].name
			label.font = .systemFont(ofSize: 30.0, weight: .black)
			label.numberOfLines = 0
			return label
		}()

		headerView.addSubview(forAutoLayout: headerLabel)

		headerLabel.snp.makeConstraints { (make) in
			make.top.equalToSuperview().offset(15)
			make.leading.equalToSuperview().offset(20)
			make.trailing.equalToSuperview().offset(-20)
			if section != 0 {
				make.bottom.equalToSuperview().offset(-15)
			}
		}

		if section == 0 {

			let updateLabel: UILabel = {
				let label = UILabel()
				label.text = "Last Update: \n\(viewModel?.time ?? "")"
				label.font = .systemFont(ofSize: 20.0, weight: .medium)
				label.numberOfLines = 0
				return label
			}()

			headerView.addSubview(forAutoLayout: updateLabel)

			updateLabel.snp.makeConstraints { (make) in
				make.top.equalTo(headerLabel.snp.bottom).offset(10)
				make.leading.equalToSuperview().offset(20)
				make.trailing.equalToSuperview().offset(-20)
				make.bottom.equalToSuperview().offset(-15)
			}

			let infoButton: UIButton = {
				let button = UIButton()
				button.setBackgroundImage(UIImage(systemName: "info.circle")?.maskWithColor(color: .systemBlue), for: .normal)
				button.addTarget(self, action: #selector(showInfo), for: .touchUpInside)
				return button
			}()

			headerView.addSubview(forAutoLayout: infoButton)

			infoButton.snp.makeConstraints { (make) in
				make.top.equalToSuperview().offset(25)
				make.trailing.equalToSuperview().offset(-25)
				make.size.equalTo(25.0)
			}
		}

		return headerView
	}

	@objc
	func showInfo() {
		let viewController = InfoViewController()
		findViewController()?.present(viewController, animated: true, completion: nil)
	}
}
