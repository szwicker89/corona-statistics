//
//  MainViewModel.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class MainViewModel: BaseViewModel {

	let overallData = BehaviorRelay<[DataSourceGroup]>(value: [])
	let countriesData = BehaviorRelay<[DataSourceGroup]>(value: [])
	var time: String?

	// MARK: - Get Data
	override func get() {
		request?.dispose()
		request = Network.provider.rx
			.request(API.all)
			.subscribe(onSuccess: { [weak self] (response) in
				guard let self = self else { return }
				let mapping = CodingManager().decode(response.data, to: Overall.self)
				self.createOverall(data: mapping?.transform())
			}, onError: { (error) in
				print(error)
			})

		var requestCountries: Disposable?
		requestCountries?.dispose()
		requestCountries = Network.provider.rx
			.request(API.countries)
			.subscribe(onSuccess: { [weak self] (response) in
				guard let self = self else { return }
				let mapping = CodingManager().decode(response.data, to: [Country].self)
				self.createCountries(data: mapping)
			}, onError: { (error) in
				print(error)
			})

		Observable
			.combineLatest(overallData.asObservable(), countriesData.asObservable())
			.subscribe(onNext: { [weak self] (overall, countries) in

				guard let self = self else { return }
				var dataSourceGroups = [DataSourceGroup]()
				dataSourceGroups.append(contentsOf: overall)
				dataSourceGroups.append(contentsOf: countries)
				self.items.accept(dataSourceGroups)
			})
			.disposed(by: bag)
	}

	// MARK: - Create DataSource
	func createOverall(data: OverallTransformed?) {
		guard let data = data else { return }
		time = data.updated
		var dataSourceItems = [DataSourceItem]()
		dataSourceItems.append(
			DataSourceItem(
				type: .overallInfoCell,
				item: OverallItem(
					headline: "Cases 🤒",
					value: data.cases,
					valuePercent: nil,
					backgroundColor: .systemRed,
					textColor: .white
				),
				itemId: UUID().uuidString
			)
		)
		dataSourceItems.append(
			DataSourceItem(
				type: .overallInfoCell,
				item: OverallItem(
					headline: "Deaths 😞",
					value: data.deaths,
					valuePercent: data.deathPercent,
					backgroundColor: .systemGray,
					textColor: .white
				),
				itemId: UUID().uuidString
			)
		)
		dataSourceItems.append(
			DataSourceItem(
				type: .overallInfoCell,
				item: OverallItem(
					headline: "Recovered 🤗",
					value: data.recovered,
					valuePercent: data.recoveredPercent,
					backgroundColor: .systemGreen,
					textColor: .white
				),
				itemId: UUID().uuidString
			)
		)

		self.overallData.accept([DataSourceGroup(name: "Corona\nWorldwide Data", items: dataSourceItems)])
	}

	func createCountries(data: [Country]?) {
		guard let data = data else { return }

		var dataSourceItems = [DataSourceItem]()
		for country in data {

			let transformed = country.transform()
			dataSourceItems.append(DataSourceItem(type: .countryCell, item: transformed, itemId: transformed.id))
		}

		countriesData.accept([DataSourceGroup(name: "All Countries", items: dataSourceItems)])
	}
}
