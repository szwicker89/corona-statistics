//
//  InfoView.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 18.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import UIKit
import SafariServices

class InfoView: BaseView<InfoViewModel> {

	// MARK: - Outlets
	let closeButton: UIButton = {
		let button = UIButton()
		button.setBackgroundImage(UIImage(systemName: "xmark")?.maskWithColor(color: UIColor(named: "csBlack")), for: .normal)
		button.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
		return button
	}()

	let thanskLabel: UILabel = {
		let label = UILabel()
		label.text = "Thanks for the API goes to EliteDaMyth. Following Link to his Website and his COVID-19 DiscordBot"
		label.font = .systemFont(ofSize: 18.0, weight: .medium)
		label.numberOfLines = 0
		return label
	}()

	let websiteButton: UIButton = {
		let button = UIButton()
		button.layer.cornerRadius = 8.0
		button.backgroundColor = .systemBlue
		button.setTitle("Website", for: .normal)
		button.setTitleColor(.white, for: .normal)
		button.addTarget(self, action: #selector(openWebsite), for: .touchUpInside)
		return button
	}()

	let apiGithubButton: UIButton = {
		let button = UIButton()
		button.layer.cornerRadius = 8.0
		button.backgroundColor = .black
		button.setTitle("API on GitHub", for: .normal)
		button.setTitleColor(.white, for: .normal)
		button.addTarget(self, action: #selector(openApiGithub), for: .touchUpInside)
		return button
	}()

	let discordButton: UIButton = {
		let button = UIButton()
		button.layer.cornerRadius = 8.0
		button.backgroundColor = .systemPurple
		button.setTitle("Discord Bot", for: .normal)
		button.setTitleColor(.white, for: .normal)
		button.addTarget(self, action: #selector(openDiscordBot), for: .touchUpInside)
		return button
	}()

	// MARK: - Layout
	override func setupViewHierarchy() {
		super.setupViewHierarchy()
		addSubviews(forAutoLayout: [
			closeButton,
			thanskLabel,
			websiteButton,
			apiGithubButton,
			discordButton
		])
	}

	override func setupViews() {
		super.setupViews()
	}

	override func setupConstraints() {
		super.setupConstraints()
		closeButton.snp.makeConstraints { (make) in
			make.top.equalToSuperview().offset(25)
			make.trailing.equalToSuperview().offset(-25)
			make.size.equalTo(20.0)
		}

		thanskLabel.snp.makeConstraints { (make) in
			make.top.equalTo(closeButton.snp.bottom).offset(20)
			make.leading.equalToSuperview().offset(25)
			make.trailing.equalToSuperview().offset(-25)
		}

		websiteButton.snp.makeConstraints { (make) in
			make.top.equalTo(thanskLabel.snp.bottom).offset(30)
			make.leading.equalToSuperview().offset(40)
			make.trailing.equalToSuperview().offset(-40)
			make.height.equalTo(50.0)
		}

		apiGithubButton.snp.makeConstraints { (make) in
			make.top.equalTo(websiteButton.snp.bottom).offset(15)
			make.leading.trailing.equalTo(websiteButton)
			make.height.equalTo(websiteButton)
		}

		discordButton.snp.makeConstraints { (make) in
			make.top.equalTo(apiGithubButton.snp.bottom).offset(15)
			make.leading.trailing.equalTo(websiteButton)
			make.height.equalTo(websiteButton)
			make.bottom.lessThanOrEqualToSuperview().offset(-40)
		}
	}

	@objc
	func dismissView() {
		findViewController()?.dismiss(animated: true, completion: nil)
	}

	@objc
	func openWebsite() {
		guard let url = URL(string: "https://lmao.ninja") else { return }
		let viewController = SFSafariViewController(url: url)
		findViewController()?.present(viewController, animated: true, completion: nil)
	}

	@objc
	func openApiGithub() {
		guard let url = URL(string: "https://github.com/NovelCOVID/API") else { return }
		let viewController = SFSafariViewController(url: url)
		findViewController()?.present(viewController, animated: true, completion: nil)
	}

	@objc
	func openDiscordBot() {
		guard let url = URL(string: "https://top.gg/bot/685268214435020809") else { return }
		let viewController = SFSafariViewController(url: url)
		findViewController()?.present(viewController, animated: true, completion: nil)
	}
}
