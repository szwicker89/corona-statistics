//
//  CountryViewModel.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class CountryViewModel: BaseViewModel {

	// MARK: - GetData
	override func get() {

	}

	func get(country: String) {

		request?.dispose()
		request = Network.provider.rx
			.request(API.country(from: country))
			.subscribe(onSuccess: { [weak self] (response) in
				guard let self = self else { return }
				let mapping = CodingManager().decode(response.data, to: Country.self)
				self.createDataSource(country: mapping?.transform())
			}, onError: { (error) in
				print(error)
			})
	}

	func createDataSource(country: CountryTransformed?) {
		guard let country = country else { return }
		var dataSourceItems = [DataSourceItem]()
		dataSourceItems.append(
			DataSourceItem(
				type: .countryDetailCell,
				item: CountryDetailItem(dataType: .cases, item: country),
				itemId: UUID().uuidString
			)
		)
		dataSourceItems.append(
			DataSourceItem(
				type: .countryDetailCell,
				item: CountryDetailItem(dataType: .deaths, item: country),
				itemId: UUID().uuidString
			)
		)
		dataSourceItems.append(
			DataSourceItem(
				type: .countryDetailCell,
				item: CountryDetailItem(dataType: .recovered, item: country),
				itemId: UUID().uuidString
			)
		)

		self.items.accept([DataSourceGroup(name: "\(country.country)", items: dataSourceItems)])
	}
}
