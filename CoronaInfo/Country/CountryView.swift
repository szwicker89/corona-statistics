//
//  CountryView.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import UIKit

class CountryView: BaseTableView<CountryViewModel> {

	override func setupViews() {
		super.setupViews()
		tableView.refreshControl = nil
		tableView.delegate = self
	}

	public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

		let headerView: UIView = {
			let view = UIView()
			return view
		}()

		let headerLabel: UILabel = {
			let label = UILabel()
			label.text = dataSource.sectionModels[section].name
			label.font = .systemFont(ofSize: 38.0, weight: .black)
			label.numberOfLines = 0
			return label
		}()

		headerView.addSubview(forAutoLayout: headerLabel)

		headerLabel.snp.makeConstraints { (make) in
			make.top.equalToSuperview().offset(15)
			make.leading.equalToSuperview().offset(20)
			make.trailing.equalToSuperview().offset(-20)
			make.bottom.equalToSuperview().offset(-15)
		}

		return headerView
	}
}

