//
//  CountryViewController.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import UIKit

class CountryViewController: BaseViewController<CountryView, CountryViewModel> {

	// MARK: - Outlets
	let closeButton: UIButton = {
		let button = UIButton()
		button.setBackgroundImage(UIImage(systemName: "xmark")?.maskWithColor(color: UIColor(named: "csBlack")), for: .normal)
		button.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
		return button
	}()

	// MARK: - Properties
	var countryName = ""

	// MARK: -LifeCycle
	override func viewDidLoad() {
		super.viewDidLoad()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		viewModel.get(country: countryName)
	}

	override func setupViewHierarchy() {
		super.setupViewHierarchy()
		view.addSubview(closeButton)
	}

	override func setupViews() {
		super.setupViews()
	}

	override func setupConstraints() {
		super.setupConstraints()
		closeButton.snp.makeConstraints { (make) in
			make.top.equalToSuperview().offset(25)
			make.trailing.equalToSuperview().offset(-25)
			make.size.equalTo(20.0)
		}
	}

	@objc
	func dismissView() {
		dismiss(animated: true, completion: nil)
	}
}
