//
//  DataSourceItem.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 01.05.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

import Foundation
import RxDataSources

struct DataSourceItem: DataSourceItemProtocol {

	// MARK: - Properties
	var type: DataSourceItemEnum
	var item: Any
	var itemId: String
}

// MARK: - IdentifiableType
extension DataSourceItem: IdentifiableType {
	var identity: String {
		return itemId
	}
}

// MARK: - Equatable
extension DataSourceItem: Equatable {
	static func == (lhs: DataSourceItem, rhs: DataSourceItem) -> Bool {
		return lhs.itemId == rhs.itemId
	}
}
