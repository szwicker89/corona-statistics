//
//  DataSourceItemGroup.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 01.05.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

import Foundation
import RxDataSources

struct DataSourceGroup {

	// MARK: - Properties
	let name: String
	var items: [Item]

	// MARK: Life-Cycle
	init(name: String, items: [Item]) {
		self.name = name
		self.items = items
	}
}

// MARK: - IdentifiableType
extension DataSourceGroup: IdentifiableType {
	var identity: String {
		return name
	}
}

// MARK: - AnimatableSectionModelType
extension DataSourceGroup: AnimatableSectionModelType {

	typealias Item = DataSourceItem

	init(original: DataSourceGroup, items: [DataSourceItem]) {

		self = original
		self.items = items
	}
}
