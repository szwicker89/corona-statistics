//
//  DataSourceItemEnum.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 01.05.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

enum DataSourceItemEnum: String {

	// MARK: - TableViewCells
	case overallInfoCell
	case countryCell
	case countryDetailCell
}
