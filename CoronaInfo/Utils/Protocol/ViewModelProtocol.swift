//
//  ViewModelProtocol.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 07.05.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol ViewModelProtocol: class {

	// Properties
	var bag: DisposeBag { get set }
	var items: BehaviorRelay<[DataSourceGroup]> { get set }

	// Get
	func get()
}
