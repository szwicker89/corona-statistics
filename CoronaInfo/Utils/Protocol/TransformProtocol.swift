//
//  TransformProtocol.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 01.05.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

protocol TransformProtocol {

	// MARK: - Properties
	associatedtype T

	// MARK: - Functions
	func transform() -> T
}
