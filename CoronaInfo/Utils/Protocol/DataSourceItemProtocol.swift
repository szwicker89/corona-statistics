//
//  DataSourceItemProtocol.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 01.05.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

import Foundation
import RxDataSources

protocol DataSourceItemProtocol {

	// MARK: - Properties
	var type: DataSourceItemEnum { get set }
	var item: Any { get set }
}
