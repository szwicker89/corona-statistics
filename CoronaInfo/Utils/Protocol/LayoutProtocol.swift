//
//  LayoutProtocol.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 30.04.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

protocol LayoutProtocol: class {

	// MARK: - Functions
	func setupLayout()
	func setupViewHierarchy()
	func setupViews()
	func setupConstraints()
}

// MARK: ProtocolExtension
extension LayoutProtocol {

	func setupLayout() {

		setupViewHierarchy()
		setupViews()
		setupConstraints()
	}
}
