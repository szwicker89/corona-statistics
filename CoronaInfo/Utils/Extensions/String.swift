//
//  String.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 01.05.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

import UIKit

extension String {

	func translate() -> String {
		return Bundle.main.localizedString(forKey: self, value: nil, table: "Translations")
	}
}
