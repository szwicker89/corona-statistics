//
//  UIImage.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 30.04.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

import UIKit

extension UIImage {

	func maskWithColor(color: UIColor?) -> UIImage? {
		defer { UIGraphicsEndImageContext() }
		guard let color = color else { return self }
		UIGraphicsBeginImageContextWithOptions(self.size, false, UIScreen.main.scale)
		guard let context = UIGraphicsGetCurrentContext() else { return self }
		color.setFill()
		context.translateBy(x: 0, y: self.size.height)
		context.scaleBy(x: 1.0, y: -1.0)
		context.setBlendMode(CGBlendMode.colorBurn)
		let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
		context.draw(self.cgImage!, in: rect)
		context.setBlendMode(CGBlendMode.sourceIn)
		context.addRect(rect)
		context.drawPath(using: CGPathDrawingMode.fill)
		return UIGraphicsGetImageFromCurrentImageContext()
	}
}
