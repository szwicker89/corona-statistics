//
//  UIViewController.swift
//  Kickscore
//
//  Created by Simon Zwicker on 26.02.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import UIKit

extension UIViewController {

	func hideKeyboardWhenTappedAround() {
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
		view.addGestureRecognizer(tap)
		tap.cancelsTouchesInView = false
	}

	@objc
	func dismissKeyboard() {
		view.endEditing(true)
	}
}
