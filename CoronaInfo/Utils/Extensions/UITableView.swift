//
//  UITableView.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 01.05.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

import UIKit

extension UITableView {

	func register(itemTypes: [DataSourceItemEnum]) {
		for type in itemTypes {
			switch type {
			case .overallInfoCell:
				register(OverallInfoCell.self, forCellReuseIdentifier: type.rawValue)

			case .countryCell:
				register(CountryCell.self, forCellReuseIdentifier: type.rawValue)

			case .countryDetailCell:
				register(CountryDetailCell.self, forCellReuseIdentifier: type.rawValue)
			}
		}
	}

	func dequeueReusableCell(item: DataSourceItemProtocol, indexPath: IndexPath) -> BaseTableViewCell {
		guard let cell = dequeueReusableCell(withIdentifier: item.type.rawValue, for: indexPath) as? BaseTableViewCell else {
			return BaseTableViewCell()
		}
		return cell
	}
}
