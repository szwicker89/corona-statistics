//
//  NSNumber.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import Foundation

extension NSNumber {

	func formatAsCurrency(digits: Int = 2, numberStyle: NumberFormatter.Style = .currency, currencyCode: String = "EUR") -> String? {

		let currencyFormatter = NumberFormatter()

		currencyFormatter.minimumFractionDigits = digits
		currencyFormatter.maximumFractionDigits = digits
		currencyFormatter.numberStyle = numberStyle
		currencyFormatter.currencyCode = currencyCode

		return currencyFormatter.string(from: self)
	}

	func format(digits: Int = 2, suffix: String? = nil) -> String? {

		let numberFormatter = NumberFormatter()
		var result: String?

		numberFormatter.minimumFractionDigits = digits
		numberFormatter.maximumFractionDigits = digits
		numberFormatter.numberStyle = .decimal

		if let formattedString = numberFormatter.string(from: self) {

			result = formattedString

			if let suffix = suffix {

				result = "\(formattedString) \(suffix)"
			}
		}

		return result
	}
}
