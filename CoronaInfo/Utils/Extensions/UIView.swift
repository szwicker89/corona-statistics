//
//  UIView.swift
//  Kickscore
//
//  Created by Simon Zwicker on 26.02.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import UIKit

extension UIView {

	func findViewController() -> UIViewController? {

		if let nextResponder = self.next as? UIViewController {

			return nextResponder
		} else if let nextResponder = self.next as? UIView {

			return nextResponder.findViewController()
		} else {

			return nil
		}
	}

	/** Adds subview and sets translatesAutoresizingMaskIntoConstraints to false
		- parameter subview: The subview to add and prepare for AutoLayout
	*/
	func addSubview(forAutoLayout subview: UIView) {
		addSubview(subview)
		subview.translatesAutoresizingMaskIntoConstraints = false
	}

	/** Adds subviews and sets translatesAutoresizingMaskIntoConstraints to false.
		- parameter subviews: The subviews to add and prepare for AutoLayout.
	*/
	func addSubviews(forAutoLayout subviews: [UIView]) {
		subviews.forEach { addSubview(forAutoLayout: $0) }
	}
}
