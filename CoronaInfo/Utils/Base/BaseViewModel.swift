//
//  BaseViewModel.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 30.04.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BaseViewModel: ViewModelProtocol {

	// MARK: - Properties
	var request: Disposable?
	var bag: DisposeBag = DisposeBag()
	var items: BehaviorRelay<[DataSourceGroup]> = BehaviorRelay<[DataSourceGroup]>(value: [])

	// MARK: - Life-Cycle
	required init() {}

	// MARK: - Data
	func get() {}
}
