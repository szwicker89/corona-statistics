//
//  BaseViewController.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 30.04.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class BaseViewController<V, VM>: UIViewController, LayoutProtocol where V: UIView, VM: BaseViewModel {

	// MARK: - Outlets
	let subview = V()
	let viewModel = VM()

	// MARK: - Properties
	let bag = DisposeBag()

	// MARK: - Life-Cycle
	override func viewDidLoad() {

		super.viewDidLoad()
		setupLayout()
		bindData()

		if let subview = subview as? BaseView<VM> {

			subview.viewModel = viewModel
			subview.setupLayout()
			subview.bindData()
		} else if let subview = subview as? BaseTableView<VM> {

			subview.viewModel = viewModel
			subview.setupLayout()
			subview.bindData()
		}
	}

	// MARK: - Data-Handling
	func bindData() {
		
	}

	// MARK: - Layout
	func setupViewHierarchy() {

		view.addSubviews(forAutoLayout: [subview])
	}

	func setupViews() {

		hideKeyboardWhenTappedAround()
		view.backgroundColor = UIColor(named: "csWhite")
	}

	func setupConstraints() {

		subview.snp.makeConstraints { (make) in
			make.top.leading.trailing.equalTo(view.safeAreaLayoutGuide)
			make.bottom.equalToSuperview()
		}
	}
}
