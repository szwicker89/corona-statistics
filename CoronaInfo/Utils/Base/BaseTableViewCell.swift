//
//  BaseTableCell.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 30.04.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell, LayoutProtocol {

	// MARK: - Life-Cycle
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Layout
	func layout(item: Any) {}
	func setupViewHierarchy() {}
	func setupViews() {}
	func setupConstraints() {}
}
