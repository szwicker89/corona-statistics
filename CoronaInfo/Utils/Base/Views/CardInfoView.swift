//
//  CardInfoView.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import UIKit

class CardInfoView: UIView, LayoutProtocol {

	// MARK: - Outlets
	let cardHeadline: UILabel = {
		let label = UILabel()
		label.textColor = .white
		label.font = .systemFont(ofSize: 24.0)
		return label
	}()

	let cardValue: UILabel = {
		let label = UILabel()
		label.textColor = .white
		label.font = .systemFont(ofSize: 26.0, weight: .black)
		return label
	}()

	let cardPercentValue: UILabel = {
		let label = UILabel()
		label.textColor = .white
		label.font = .systemFont(ofSize: 18.0, weight: .medium)
		return label
	}()

	// MARK: - LifeCycle
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupLayout()
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
	}

	// MARK: - Setup
	func setup(headline: String, value: String, valuePercent: String?, backgroundColor: UIColor?, textColor: UIColor?) {
		cardHeadline.text = headline
		cardValue.text = value
		cardPercentValue.text = valuePercent
		cardHeadline.textColor = textColor
		cardValue.textColor = textColor
		cardPercentValue.textColor = textColor
		self.backgroundColor = backgroundColor
	}

	// MARK: - Layout
	func setupViewHierarchy() {
		addSubviews(forAutoLayout: [
			cardHeadline,
			cardValue,
			cardPercentValue
		])
	}

	func setupViews() {
		layer.cornerRadius = 10.0
	}

	func setupConstraints() {

		cardHeadline.snp.makeConstraints { (make) in
			make.top.leading.equalToSuperview().offset(15)
			make.trailing.equalToSuperview().offset(-15)
		}

		cardValue.snp.makeConstraints { (make) in
			make.top.equalTo(cardHeadline.snp.bottom).offset(15)
			make.leading.equalTo(cardHeadline)
			make.bottom.equalToSuperview().offset(-15)
		}

		cardPercentValue.snp.makeConstraints { (make) in
			make.leading.equalTo(cardValue.snp.trailing).offset(15)
			make.trailing.equalToSuperview().offset(-15)
			make.centerY.equalTo(cardValue)
		}
	}
}
