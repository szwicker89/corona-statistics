//
//  CardDetailInfoView.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import UIKit

enum DetailCardEnum {

	case cases
	case deaths
	case recovered
}

class CardDetailInfoView: UIView, LayoutProtocol {

	// MARK: - Outlets
	let cardHeadline: UILabel = {
		let label = UILabel()
		label.textColor = .white
		label.font = .systemFont(ofSize: 24.0)
		return label
	}()

	let cardValue: UILabel = {
		let label = UILabel()
		label.textColor = .white
		label.font = .systemFont(ofSize: 26.0, weight: .black)
		return label
	}()

	let cardPercentValue: UILabel = {
		let label = UILabel()
		label.textColor = .white
		label.font = .systemFont(ofSize: 18.0, weight: .medium)
		return label
	}()

	let todayStack: UIStackView = {
		let stackView = UIStackView()
		stackView.distribution = .equalSpacing
		stackView.axis = .horizontal
		stackView.spacing = 10.0
		return stackView
	}()

	let todayLabel: UILabel = {
		let label = UILabel()
		label.textColor = .white
		label.font = .systemFont(ofSize: 20.0, weight: .bold)
		return label
	}()

	let todayValue: UILabel = {
		let label = UILabel()
		label.textColor = .white
		label.textAlignment = .right
		label.font = .systemFont(ofSize: 20.0, weight: .medium)
		return label
	}()

	let todayPercent: UILabel = {
		let label = UILabel()
		label.textColor = .white
		label.textAlignment = .right
		label.font = .systemFont(ofSize: 20.0, weight: .medium)
		return label
	}()

	// MARK: - LifeCycle
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupLayout()
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
	}

	// MARK: - Setup
	func setup(detailInfo: DetailCardEnum, item: CountryTransformed) {

		switch detailInfo {
		case .cases:
			self.backgroundColor = .systemRed
			cardHeadline.text = "Cases 🤒"
			cardValue.text = item.cases
			cardPercentValue.text = "Critical: \(item.critical)"
			todayLabel.text = "Today:"
			todayValue.text = item.todayCases
			todayPercent.text = item.todayCasesGrowth

		case .deaths:
			self.backgroundColor = .systemGray
			cardHeadline.text = "Deaths 😞"
			cardValue.text = item.deaths
			cardPercentValue.text = item.deathsPercent
			todayLabel.text = "Today:"
			todayValue.text = item.todayDeaths
			todayPercent.text = item.todayDeathsGrowth

		case .recovered:
			self.backgroundColor = .systemGreen
			cardHeadline.text = "Recovered 🤗"
			cardValue.text = item.recovered
			cardPercentValue.text = item.recoveredPercent
		}

		for subview in self.subviews {
			if let label = subview as? UILabel {
				label.textColor = .white
			}
		}
	}

	// MARK: - Layout
	func setupViewHierarchy() {
		addSubviews(forAutoLayout: [
			cardHeadline,
			cardValue,
			cardPercentValue,
			todayStack
		])

		todayStack.addArrangedSubview(todayLabel)
		todayStack.addArrangedSubview(todayValue)
		todayStack.addArrangedSubview(todayPercent)
	}

	func setupViews() {
		layer.cornerRadius = 10.0
	}

	func setupConstraints() {

		cardHeadline.snp.makeConstraints { (make) in
			make.top.leading.equalToSuperview().offset(15)
			make.trailing.equalToSuperview().offset(-15)
		}

		cardValue.snp.makeConstraints { (make) in
			make.top.equalTo(cardHeadline.snp.bottom).offset(15)
			make.leading.equalTo(cardHeadline)
		}

		cardPercentValue.snp.makeConstraints { (make) in
			make.leading.equalTo(cardValue.snp.trailing).offset(15)
			make.trailing.equalToSuperview().offset(-15)
			make.centerY.equalTo(cardValue)
		}

		todayStack.snp.makeConstraints { (make) in
			make.top.equalTo(cardValue.snp.bottom).offset(20)
			make.leading.trailing.equalTo(cardHeadline)
			make.bottom.equalToSuperview().offset(-15)
		}
	}
}

