//
//  File.swift
//  AnnoGuide
//
//  Created by Simon Zwicker on 30.04.19.
//  Copyright © 2019 Simon Zwicker. All rights reserved.
//

import UIKit
import RxSwift

class BaseView<VM>: UIView, LayoutProtocol where VM: BaseViewModel {

	// MARK: - Properties
	let bag = DisposeBag()
	var viewModel: VM?

	// MARK: - Data-Handling
	func bindData() {

	}

	// MARK: - Layout
	func setupViewHierarchy() {

	}

	func setupViews() {
		backgroundColor = .systemFill
	}

	func setupConstraints() {

	}
}
