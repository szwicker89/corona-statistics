//
//  BaseTableView.swift
//  FoosballScore
//
//  Created by Simon Zwicker on 06.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

class BaseTableView<VM>: UIView, UITableViewDelegate, LayoutProtocol where VM: BaseViewModel {

	// MARK: - Outlets
	let tableView = UITableView()
	var dataSource: RxTableViewSectionedAnimatedDataSource<DataSourceGroup>!
	let refresh = UIRefreshControl()
	
	// MARK: - Properties
	let bag = DisposeBag()
	var viewModel: VM?

	// MARK: - Data-Handling
	func bindData() {

		dataSource = RxTableViewSectionedAnimatedDataSource<DataSourceGroup>(configureCell: { (_, tableView, indexPath, item) -> UITableViewCell in
			let cell = tableView.dequeueReusableCell(item: item, indexPath: indexPath)
			cell.layout(item: item.item)

			return cell
		})

		viewModel?.items.asObservable()
			.bind(to: tableView.rx.items(dataSource: dataSource))
			.disposed(by: bag)
	}

	// MARK: - Layout
	func setupViewHierarchy() {
		addSubview(forAutoLayout: tableView)
	}

	func setupViews() {
		tableView.register(itemTypes: [.overallInfoCell, .countryCell, .countryDetailCell])
		tableView.rx.setDelegate(self).disposed(by: bag)
		tableView.tableFooterView = UIView()
		tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
		tableView.refreshControl = refresh
	}

	func setupConstraints() {
		tableView.snp.makeConstraints { (make) in
			make.edges.equalToSuperview()
		}
	}

	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return UITableView.automaticDimension
	}

	open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

		let headerView: UIView = {
			let view = UIView()
			view.backgroundColor = UIColor(named: "csWhite")
			return view
		}()

		let headerLabel: UILabel = {
			let label = UILabel()
			label.text = dataSource.sectionModels[section].name
			label.font = .systemFont(ofSize: 38.0, weight: .black)
			label.numberOfLines = 0
			return label
		}()

		headerView.addSubview(forAutoLayout: headerLabel)

		headerLabel.snp.makeConstraints { (make) in
			make.top.equalToSuperview().offset(15)
			make.leading.equalToSuperview().offset(20)
			make.trailing.equalToSuperview().offset(-20)
			make.bottom.equalToSuperview().offset(-15)
		}

		return headerView
	}
}
