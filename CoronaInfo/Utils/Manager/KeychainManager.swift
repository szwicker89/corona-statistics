//
//  KeychainManager.swift
//  FoosballScore
//
//  Created by Simon Zwicker on 06.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import Foundation
import KeychainSwift

class KeychainManager {

	// MARK: - Properties
	let keychain = KeychainSwift()

	// MARK: - Initialization
	init() {
		keychain.synchronizable = true
	}

	// MARK: - Setter
	func set(_ bool: Bool, for identifier: KeychainIdentifierEnum) {
		keychain.set(bool, forKey: identifier.rawValue)
	}

	func set(_ object: Data, for identifier: KeychainIdentifierEnum) {
		keychain.set(object, forKey: identifier.rawValue)
	}

	func set(_ string: String, for identifier: KeychainIdentifierEnum) {
		keychain.set(string, forKey: identifier.rawValue)
	}

	// MARK: - Getter
	func getBool(for identifier: KeychainIdentifierEnum) -> Bool {
		return keychain.getBool(identifier.rawValue) ?? false
	}

	func getString(for identifier: KeychainIdentifierEnum) -> String? {
		return keychain.get(identifier.rawValue)
	}

	func getData(for identifier: KeychainIdentifierEnum) -> Data? {
		return keychain.getData(identifier.rawValue)
	}

	// MARK: - Remove
	func delete(for identifier: KeychainIdentifierEnum) {
		keychain.delete(identifier.rawValue)
	}

	// MARK: - RemoveAll
	func removeAll() {
		keychain.clear()
	}
}
