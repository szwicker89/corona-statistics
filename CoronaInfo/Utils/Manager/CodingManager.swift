//
//  CodingManager.swift
//  FoosballScore
//
//  Created by Simon Zwicker on 06.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import Foundation

class CodingManager {

	// MARK: - Properties
	let encoder = JSONEncoder()
	let decoder = JSONDecoder()

	// MARK: - Encoding
	func encode<T: Encodable>(_ object: T) -> Data? {
		return try? encoder.encode(object)
	}

	// MARK: - Decode
	func decode<T: Decodable>(_ object: Data, to: T.Type) -> T? {
		return try? decoder.decode(T.self, from: object)
	}
}
