//
//  CountryDetailItem.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import Foundation

struct CountryDetailItem {
	let dataType: DetailCardEnum
	let item: CountryTransformed
}
