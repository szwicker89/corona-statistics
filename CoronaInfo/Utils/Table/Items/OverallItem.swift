//
//  OverallItem.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import UIKit

struct OverallItem {

	let headline: String
	let value: String
	let valuePercent: String?
	let backgroundColor: UIColor?
	let textColor: UIColor?
}
