//
//  CountryCell.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import UIKit

class CountryCell: BaseTableViewCell {
	typealias T = CountryTransformed

	// MARK: - Outlets
	let valueStack: UIStackView = {
		let view = UIStackView()
		view.alignment = .center
		view.distribution = .fillEqually
		view.axis = .horizontal
		view.spacing = 15.0
		return view
	}()

	let name: UILabel = {
		let label = UILabel()
		label.font = .systemFont(ofSize: 22.0, weight: .bold)
		label.numberOfLines = 0
		return label
	}()

	let caseValue: UILabel = {
		let label = UILabel()
		label.font = .systemFont(ofSize: 22.0, weight: .medium)
		label.numberOfLines = 0
		label.textAlignment = .right
		return label
	}()

	// MARK: - Layout
	override func layout(item: Any) {
		guard let item = item as? T else { return }
		configure(item: item)
		setupLayout()
	}

	override func setupViewHierarchy() {
		super.setupViewHierarchy()

		contentView.addSubviews(forAutoLayout: [
			valueStack
		])

		valueStack.addArrangedSubview(name)
		valueStack.addArrangedSubview(caseValue)
	}

	override func setupViews() {
		super.setupViews()
	}

	override func setupConstraints() {
		super.setupConstraints()

		valueStack.snp.makeConstraints { (make) in
			make.top.leading.equalToSuperview().offset(15)
			make.bottom.trailing.equalToSuperview().offset(-15)
		}
	}

	// MARK: - Configure Cell
	func configure(item: T) {

		accessoryType = .disclosureIndicator

		name.text = item.country
		caseValue.text = "\(item.cases) 🤒"
	}
}
