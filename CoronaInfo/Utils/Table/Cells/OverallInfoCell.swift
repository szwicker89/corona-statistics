//
//  OverallInfoCell.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import UIKit

class OverallInfoCell: BaseTableViewCell {
	typealias T = OverallItem

	// MARK: - Outlets
	let cardInfoView: CardInfoView = {
		let view = CardInfoView()
		return view
	}()

	// MARK: - Layout
	override func layout(item: Any) {
		guard let item = item as? T else { return }
		configure(item: item)
		setupLayout()
	}

	override func setupViewHierarchy() {
		super.setupViewHierarchy()
		contentView.addSubview(forAutoLayout: cardInfoView)
	}

	override func setupConstraints() {
		super.setupConstraints()

		cardInfoView.snp.makeConstraints { (make) in
			make.top.equalToSuperview().offset(10)
			make.leading.equalToSuperview().offset(20)
			make.trailing.equalToSuperview().offset(-20)
			make.bottom.equalToSuperview().offset(-10)
		}
	}

	// MARK: - Configure Cell
	func configure(item: T) {

		selectionStyle = .none
		separatorInset = UIEdgeInsets(top: 0, left: UIScreen.main.bounds.width, bottom: 0, right: 0)
		isUserInteractionEnabled = false

		cardInfoView.setup(
			headline: item.headline,
			value: item.value,
			valuePercent: item.valuePercent,
			backgroundColor: item.backgroundColor,
			textColor: item.textColor
		)
	}
}

