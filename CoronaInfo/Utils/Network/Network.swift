//
//  Network.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import Foundation
import Moya
import RxMoya

class Network {

	// MARK: - Properties
	private static let plugins = [ NetworkLoggerPlugin(configuration: NetworkLoggerPlugin.Configuration(logOptions: .verbose)) ]

	// MARK: - LifeCycle
	private init() {}

	// MARK: - FinanzenNetAPI
	static let provider = MoyaProvider<API>(
		endpointClosure: MoyaProvider.defaultEndpointMapping,
		plugins: plugins
	)
}
