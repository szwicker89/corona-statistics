//
//  API.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import Foundation
import Moya

enum API {
	case all
	case countries
	case country(from: String)
}

extension API: TargetType {

	// Build BaseURL with GlobalSettings
	var baseURL: URL {
		// Return BaseURL
		return URL(string: "https://corona.lmao.ninja")!
	}

	// Return Path for call
	var path: String {

		switch self {

		case .all:
			return "/all"

		case .countries:
			return "/countries"

		case .country(let country):
			return "/countries/\(country)"
		}
	}

	// Return Method for call
	var method: Moya.Method {

		switch self {

		default:
			return .get
		}
	}

	// Return Parameters for Call
	var parameters: [String: Any] {
		return [String: Any]()
	}

	// Return RequestOption with Parameters & Encoding for Call
	var task: Task {
		return .requestPlain
	}

	// Return Sample Data for Stubbing for UI & Unit Testing
	var sampleData: Data { return Data() }

	// Return Headers for Call
	var headers: [String: String]? {
		return nil
	}

	// Return Finished Request URL
	var url: String {
		return "\(baseURL)\(path)"
	}
}
