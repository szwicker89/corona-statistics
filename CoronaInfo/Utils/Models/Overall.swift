//
//  Overall.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import Foundation

struct Overall: Codable {

	let cases: Int?
	let deaths: Int?
	let recovered: Int?
	let updated: TimeInterval?
}
