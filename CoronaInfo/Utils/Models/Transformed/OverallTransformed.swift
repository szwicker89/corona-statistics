//
//  OverallTransformed.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import Foundation
import SwiftDate

struct OverallTransformed {

	let id = UUID().uuidString
	let cases: String
	let deaths: String
	let deathPercent: String
	let recovered: String
	let recoveredPercent: String
	let updated: String
}

extension Overall: TransformProtocol {
	typealias T = OverallTransformed

	func transform() -> T {
		let casesString = NSNumber(value: self.cases ?? 0).format(digits: 0, suffix: nil) ?? ""
		let deathsString = NSNumber(value: self.deaths ?? 0).format(digits: 0, suffix: nil) ?? ""
		var deathPercentString = ""
		let recoveredString = NSNumber(value: self.recovered ?? 0).format(digits: 0, suffix: nil) ?? ""
		var recoveredPercentString = ""
		var updatedString = ""

		if let cases = self.cases, let deaths = self.deaths {
			let deathPercent = (Float(deaths) / Float(cases)) * 100.0
			deathPercentString = NSNumber(value: deathPercent).format(digits: 2, suffix: "%") ?? ""
		}

		if let cases = self.cases, let recovered = self.recovered {
			let recoveredPercent = (Float(recovered) / Float(cases)) * 100.0
			recoveredPercentString = NSNumber(value: recoveredPercent).format(digits: 2, suffix: "%") ?? ""
		}

		if let time = self.updated {

			let string = NSNumber(value: time).stringValue
			let intervalString = string.dropLast(3)
			let correctInterval = (intervalString as NSString).doubleValue
			let date = Date(timeIntervalSince1970: correctInterval)
			let dateFormatter = DateFormatter()
			dateFormatter.locale = Locale(identifier:Locale.current.identifier)
			dateFormatter.dateStyle = .long
			dateFormatter.timeStyle = .medium
			updatedString = dateFormatter.string(from: date)
		}

		return OverallTransformed(
			cases: casesString,
			deaths: deathsString,
			deathPercent: deathPercentString,
			recovered: recoveredString,
			recoveredPercent: recoveredPercentString,
			updated: updatedString
		)
	}
}
