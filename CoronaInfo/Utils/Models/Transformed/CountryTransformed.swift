//
//  CountryTransformed.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

import Foundation

struct CountryTransformed {

	let id = UUID().uuidString
	let country: String
	let cases: String
	let todayCases: String
	let todayCasesGrowth: String
	let deaths: String
	let deathsPercent: String
	let todayDeaths: String
	let todayDeathsGrowth: String
	let recovered: String
	let recoveredPercent: String
	let critical: String
}

extension Country: TransformProtocol {
	typealias T = CountryTransformed

	func transform() -> T {
		let country = self.country ?? ""
		let casesString = NSNumber(value: self.cases ?? 0).format(digits: 0, suffix: nil) ?? ""
		let todayCasesString = NSNumber(value: self.todayCases ?? 0).format(digits: 0, suffix: nil) ?? ""
		var todayCasesGrowth = ""
		let deathsString = NSNumber(value: self.deaths ?? 0).format(digits: 0, suffix: nil) ?? ""
		var deathPercentString = ""
		let todayDeathsString = NSNumber(value: self.todayDeaths ?? 0).format(digits: 0, suffix: nil) ?? ""
		var todayDeathsGrowth = ""
		let recoveredString = NSNumber(value: self.recovered ?? 0).format(digits: 0, suffix: nil) ?? ""
		var recoveredPercentString = ""
		let criticalString = NSNumber(value: self.critical ?? 0).format(digits: 0, suffix: nil) ?? ""

		if let cases = self.cases, let deaths = self.deaths {
			let deathPercent = (Float(deaths) / Float(cases)) * 100.0
			deathPercentString = NSNumber(value: deathPercent).format(digits: 2, suffix: "%") ?? ""
		}

		if let cases = self.cases, let recovered = self.recovered {
			let recoveredPercent = (Float(recovered) / Float(cases)) * 100.0
			recoveredPercentString = NSNumber(value: recovered > 0 ? recoveredPercent: 0.0).format(digits: 2, suffix: "%") ?? ""
		}

		if let cases = self.cases, let todayCases = self.todayCases {
			let yesterday = cases - todayCases
			let todayGrowth = (Float(todayCases) / Float(yesterday)) * 100.0
			todayCasesGrowth = NSNumber(value: cases > 0 ? todayGrowth: 0.0).format(digits: 2, suffix: "%") ?? ""

			if todayGrowth > 0.0 {
				todayCasesGrowth = "+ \(todayCasesGrowth)"
			}
		}

		if let deaths = self.deaths, let todayDeaths = self.todayDeaths {
			let yesterday = deaths - todayDeaths
			let todayGrowth = (Float(todayDeaths) / Float(yesterday)) * 100.0
			todayDeathsGrowth = NSNumber(value: deaths > 0 ? todayGrowth: 0.0).format(digits: 2, suffix: "%") ?? ""

			if todayGrowth > 0.0 {
				todayDeathsGrowth = "+ \(todayDeathsGrowth)"
			}
		}

		return CountryTransformed(
			country: country,
			cases: casesString,
			todayCases: todayCasesString,
			todayCasesGrowth: todayCasesGrowth,
			deaths: deathsString,
			deathsPercent: deathPercentString,
			todayDeaths: todayDeathsString,
			todayDeathsGrowth: todayDeathsGrowth,
			recovered: recoveredString,
			recoveredPercent: recoveredPercentString,
			critical: criticalString
		)
	}
}
