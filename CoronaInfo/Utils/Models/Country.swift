//
//  Country.swift
//  CoronaInfo
//
//  Created by Simon Zwicker on 17.03.20.
//  Copyright © 2020 Simon Zwicker. All rights reserved.
//

struct Country: Codable {

	let country: String?
	let cases: Int?
	let todayCases: Int?
	let deaths: Int?
	let todayDeaths: Int?
	let recovered: Int?
	let critical: Int?
}
